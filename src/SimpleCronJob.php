<?php


namespace MichalKubis\SimpleCron;


use Cron\CronExpression;
use DateTime;


class SimpleCronJob
{

	/** @var CronExpression */
	private $expression;

	/** @var IJob  */
	private $job;


	public function __construct(string $cron, IJob $job)
	{
		$this->expression = CronExpression::factory($cron);
		$this->job = $job;
	}

	public function isDue(DateTime $dateTime): bool
	{
		return $this->expression->isDue($dateTime);
	}

	public function getExpression(): CronExpression
	{
		return $this->expression;
	}

	public function execute(): void
	{
		if($this->isDue(new DateTime()))
		{
			$this->job->run();
		}
	}
}