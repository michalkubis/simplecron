<?php declare(strict_types = 1);


namespace MichalKubis\SimpleCron\DI;


use MichalKubis\SimpleCron\SimpleCron;
use MichalKubis\SimpleCron\SimpleCronException;
use Nette\DI\CompilerExtension;
use Nette\DI\Helpers;
use Nette\DI\Statement;


class SimpleCronExtension extends CompilerExtension
{

	/** @var mixed[] */
	private $defaults = [
		'jobs' => []
	];

	/**
	 * Register services
	 * @throws SimpleCronException
	 */
	public function loadConfiguration(): void
	{
		$builder = $this->getContainerBuilder();
		$config = $this->validateConfig($this->defaults);
		$config = Helpers::expand($config, $builder->parameters);

		// SimplCron
		$simpleCron = $builder->addDefinition($this->prefix('simplecron'))
			->setClass(SimpleCron::class);

		// Jobs
		foreach ($config['jobs'] as $key => $job) {
			if(!class_exists($job['class']))
			{
				throw new SimpleCronException(sprintf('Given class %s does not exist', $job['class']));
			}

			$cronJob = new Statement($job['class']);

			$simpleCron->addSetup('add', [$cronJob, $job['cron']]);
		}
	}
}