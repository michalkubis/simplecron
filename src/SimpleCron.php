<?php


namespace MichalKubis\SimpleCron;


class SimpleCron
{
	/** @var SimpleCronJob[] */
	protected $jobs = [];

	public function run(): void
	{
		$jobs = $this->jobs;
		foreach ($jobs as $job) {
			/** @var SimpleCronJob $job */
			$job->execute();
		}
	}

	/**
	 * @param IJob $job
	 * @param string $cron
	 */
	public function add(IJob $job, string $cron): void
	{
		$this->jobs[] = new SimpleCronJob($cron, $job);
	}

	/**
	 * @param string|int $key
	 * @return null|SimpleCronJob
	 */
	public function get($key): ?SimpleCronJob
	{
		return $this->jobs[$key] ?? null;
	}

	/**
	 * @return SimpleCronJob[]
	 */
	public function getAll(): array
	{
		return $this->jobs;
	}

	/**
	 * @param string|int $key
	 */
	public function remove($key): void
	{
		unset($this->jobs[$key]);
	}

	public function removeAll(): void
	{
		$this->jobs = [];
	}

}